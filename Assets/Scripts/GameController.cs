﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public enum GameState
{
    Pregame,
    Running,
    Paused,
    Postgame
}

public class GameController : MonoBehaviour
{
    private static GameController _instance;
    public static GameController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GameController>();
                DontDestroyOnLoad(_instance);
            }

            return _instance;
        }
    }

    public UiController UiController;

    public GameState State = GameState.Pregame;
    public Ball Ball;
    public PlayerController Player1;
    public PlayerController Player2;

    public void Start()
    {
        Pregame();
    }


    #region GameState control
    /// <summary>
    /// When scene is first loaded
    /// </summary>
    public void Pregame()
    {
        State = GameState.Pregame;
        UiController.PregamePanel.Show();
    }

    /// <summary>
    /// Start the game
    /// </summary>
    public void StartGame()
    {
        if (State == GameState.Pregame)
        {

            // kick ball in random dir
            var rnd = Random.value < .5f;
            var dir = (rnd ? 1f : -1f) * Vector3.right;
            Ball.Kick(dir);

            // hide pregame UI
            UiController.PregamePanel.Hide();
            State = GameState.Running;
        }
        else
        {
            throw new UnityException("Tried to start game from invalid state");
        }
    }

    /// <summary>
    /// Pause the game
    /// </summary>
    public void Pause()
    {
        if (State == GameState.Running)
        {
            // show paused UI
            UiController.PausedPanel.Show();
            // pause game timer
            // TODO
            // set paused
            State = GameState.Paused;
        }
        else
        {
            throw new UnityException("Tried to pause game from invalid state");
        }
    }

    /// <summary>
    /// Unpause the game
    /// </summary>
    public void Unpause()
    {
        if (State == GameState.Paused)
        {
            // hide pause UI
            UiController.PausedPanel.Hide();
            // continue game timer
            // TODO
            // set to running
            State = GameState.Running;
        }
        else
        {
            throw new UnityException("Tried to unpause game from invalid state");
        }
    }

    /// <summary>
    /// After postgame dialogs, do something
    /// </summary>
    public void PostGameComplete()
    {
        // TODO: go to main menu, once that's implemented
        // for now, just restart
        SceneManager.LoadScene(Scenes.MainGame);
    }
    #endregion
}
