﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UiController : MonoBehaviour
{
    public RectTransform PregamePanel;
    public RectTransform Hud;
    public RectTransform PausedPanel;
    public RectTransform PostgamePanel;
}

static class UiExtentions
{
    public static void Show(this RectTransform self)
    {
        self.gameObject.SetActive(true);
    }

    public static void Hide(this RectTransform self)
    {
        // TODO: might need to reset some variables, depending on panel
        self.gameObject.SetActive(false);
    }
}