﻿public class Axes
{
    public const string Horizontal1 = "Horizontal 1";
    public const string Horizontal2 = "Horizontal 2";
    public const string Vertical1 = "Vertical 1";
    public const string Vertical2 = "Vertical 2";
}

public class Scenes
{
    public const string MainMenu = "MainMenu";
    public const string MainGame = "Main";  // TODO temporary for game prototype 
}

public class Tags
{
    public const string Ball = "Ball";
}