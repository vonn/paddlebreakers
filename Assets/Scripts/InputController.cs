﻿using UnityEngine;
using System.Collections;

public class InputController : MonoBehaviour
{
    [SerializeField]
    protected string AxisY;
    [SerializeField]
    protected string AxisX;

    public bool Override = false;

    public float Vertical;
    public float Horizontal;

    public void UpdateAxes()
    {
        if (!Override)
        {
            Vertical = Input.GetAxisRaw(AxisY);
            Horizontal = Input.GetAxisRaw(AxisX);
        }
    }

    public void Update()
    {
        // Pause; TODO: abstract keybind
        if (Input.GetKeyDown(KeyCode.Return))
        {
            HandleReturn();
        }
    }

    static void HandleReturn()
    {
        var gc = GameController.Instance;
        switch (gc.State)
        {
            // Pregame = Start Game
            case GameState.Pregame:
                gc.StartGame();
                break;
            // Running = Pause
            case GameState.Running:
                //gc.Pause();
                break;
            // Paused = Unpause
            case GameState.Paused:
                gc.Unpause();
                break;
            // Postgame = Complete
            case GameState.Postgame:
                gc.PostGameComplete();
                break;
            default:
                throw new UnityException("Unknown gamestate when handling return");
        }
    }
}
