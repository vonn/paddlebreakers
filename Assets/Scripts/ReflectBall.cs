﻿using UnityEngine;
using System.Collections;

public class ReflectBall : MonoBehaviour
{
    void OnCollisionEnter(Collision collision)
    {
        var cp = collision.contacts[0];
        // if we've collided with the ball
        if (collision.collider.CompareTag(Tags.Ball))
        {
            // get the ball
            var ball = collision.gameObject.GetComponent<Ball>();
            // reflect it
            ball.ReflectVelocity(cp.normal);
        }
    }
}
