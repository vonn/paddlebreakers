﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
    private Rigidbody _rb;
    [SerializeField]
    private float baseVelocity = 5f;
    
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    public void Kick(Vector3 dir)
    {
        _rb.velocity = dir * baseVelocity;
    }

    /// <summary>
    /// After colliding with a surface, we reflect our velocity
    /// </summary>
    public void ReflectVelocity(Vector3 normal)
    {
        _rb.velocity = (Vector3.Reflect(_rb.velocity, normal)).normalized * baseVelocity;
    }
}
