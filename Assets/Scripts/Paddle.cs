﻿using UnityEngine;
using System.Collections;

public class Paddle : MonoBehaviour
{
    private Rigidbody _rb;
    [SerializeField]
    private float baseVelocity = 20f;

    void Start()
    {
        _rb = GetComponent<Rigidbody>();
    }

    public void Move(Vector3 dir)
    {
        // TODO any additional movement modifiers
        _rb.velocity = dir * baseVelocity;
    }
}
