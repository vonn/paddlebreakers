﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    public Paddle Paddle;
    public InputController InputController;
    
	void Update () {
        InputController.UpdateAxes();
        MovePaddle();
    }

    public void MovePaddle()
    {
        var dir = new Vector3
        {
            x = 0,
            y = 0,
            z = InputController.Vertical
        };

        Paddle.Move(dir);
    }
}
